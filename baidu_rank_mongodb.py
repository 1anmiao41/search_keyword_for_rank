from mongodb_io import MongodbIO

# database & collection 名稱
DB_NAME = 'check-list-database'
PERF_COLL_NAME = 'preference'
DOMAIN_COLL_NAME = 'domain-info-collection-v2'
KEYWORD_COLL_NAME = 'brand_keyword-collection-v2'
RANK_COLL_NAME = 'baidu_ranking_update'


# 參數：無
# 功能：取得 db 中所有品牌名稱
# 回傳：品牌名列表 [ 品牌(string) ]
def get_brand_list():
    coll = MongodbIO(DB_NAME, PERF_COLL_NAME)
    data = coll.get_doc()
    return data[0]['brands']


# 參數：brand: 品牌名(string)
# 功能：取得該品牌旗下所有域名
# 回傳：域名列表 [ 域名(string) ]
def get_domain_list(brand):
    coll = MongodbIO(DB_NAME, DOMAIN_COLL_NAME)
    data = coll.get_distinct('domain_name', { 'brand': brand }) # todo: 缺 'is_deleted': False
    return data


# 參數：brand: 品牌名(string)
# 功能：取得該品牌旗下所有關鍵字
# 回傳：關鍵字列表 [ 關鍵字(string) ]
def get_keyword_list(brand):
    print(brand)
    pref_coll = MongodbIO(DB_NAME, PERF_COLL_NAME)
    data = pref_coll.get_doc()
    keyword_serial_number = data[0]['keyword_serial_number']
    keyword_coll = MongodbIO(DB_NAME, KEYWORD_COLL_NAME)
    keyword_list = keyword_coll.get_doc({ 'keyword_serial_number': keyword_serial_number, 'brand': brand })
    if keyword_list.count():
        return keyword_list[0]['brand_keywords']
    else:
        return []


# 參數：keyword: 關鍵字(string)
#       rank_list: 域名出現在搜尋結果上的排名紀錄列表 [ { 'rank': 排名(int), 'domain': 域名(string), 'time': 搜尋時間('%Y-%m-%d %H:%M:%S') } ]
# 功能：將排名紀錄輸入至 db
# 回傳：無
def rank_update(keyword, rank_list):
    pref_coll = MongodbIO(DB_NAME, PERF_COLL_NAME)
    data = pref_coll.get_doc()
    serial_num = data[0]['rank_serial_number']
    data_list = []
    for rank in rank_list:
        data = { 'domain_name': rank['domain'], 'ranking_keyword': keyword, 'rank_serial_number': serial_num, 'type': 1, 'rank': rank['rank'], 'update_time': rank['time'] }
        # print(data)
        data_list.append(data)
    rank_coll = MongodbIO(DB_NAME, RANK_COLL_NAME)
    rank_coll.insert_or_update(data_list)


# 參數：無
# 功能：將 preference 中 rank_serial_number 的值更新
# 回傳：rank_serial_number 更新後的值(int)
def update_rank_serial_num():
    pref_coll = MongodbIO(DB_NAME, PERF_COLL_NAME)
    data = pref_coll.get_doc()
    new_serial_num = data[0]['rank_serial_number'] + 1
    pref_coll.set_item_value('rank_serial_number', new_serial_num, { })
    return new_serial_num


# 測試區
def main():
    
    # collection = MongodbIO('check-list-database', 'ian_test')
    # a = collection.get_doc({'_id': '1'})
    # for index in a:
    #     print(index)
    
    pref_coll = MongodbIO(DB_NAME, PERF_COLL_NAME)
    data = pref_coll.get_doc()
    serial_num = data[0]['rank_serial_number'] + 1
    data_list = []
    data = { 'domain_name': 'google.cn', 'ranking_keyword': '谷歌中國', 'rank_serial_number': 5, 'type': 1, 'rank': 1, 'update_time': '2019-04-18 11:53:19' }
    print(data)
    data_list.append(data)
    rank_coll = MongodbIO(DB_NAME, RANK_COLL_NAME)
    rank_coll.insert_or_update(data_list)


if __name__ == '__main__':
    main()