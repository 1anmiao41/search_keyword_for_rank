from pymongo import MongoClient


class MongodbConn:

    # 連接資料庫並進行初始化
    def __init__(self, db_name):
        self.db_ip = 'gu111.asuscomm.com'
        self.db_port = 45068
        self.db_name = db_name
        try:
            self.client = MongoClient(self.db_ip, self.db_port)
            # print(self.client)
        except Exception as e:
            print(e)

    # 連接資料庫，回傳資料表cursor
    def collection(self, coll_name):
        db = self.client[self.db_name]
        collection = db[coll_name]
        return collection

    # 關閉資料庫連結
    def release(self):
        self.client.close()


# 測試區
def main():
    conn = MongodbConn('check-list-database')
    coll = conn.collection('ian_test')
    x = coll.update_many({}, { '$set': { 'a': '123' } })
    print(x.modified_count, 'documents updated.')
    print(x)


if __name__ == '__main__':
    main()
