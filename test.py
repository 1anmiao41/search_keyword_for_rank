

# headers = {
#         'pragma': 'no-cache',
#         'accept-encoding': 'gzip, deflate, br',
#         'accept-language': 'zh-CN,zh;q=0.8',
#         'upgrade-insecure-requests': '1',
#         'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
#         'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#         'cache-control': 'no-cache',
#         'connection': 'keep-alive',
# }
# data = {
#     'ie': 'utf-8',
#     'wd': '万博体育',
#     'pn': 0,
#     'rn': 10
# }

# r = requests.get('https://www.baidu.com/s', params=data, headers=headers)
# html = etree.HTML(r.text, etree.HTMLParser())
# result_blocks = html.xpath('//*[@class="result c-container"]')
# print(len(result_blocks))
# for i in range(len(result_blocks)):
#     a = result_blocks[i].xpath('/*[@class="c-showurl"]')
#     print(len(a))
#     print(result_blocks[i].xpath('string()'))

# r = requests.get('https://www.baidu.com/s', params=data, headers=headers)
# html = etree.HTML(r.text, etree.HTMLParser())
# result_blocks = html.xpath('//h3')
# print(len(result_blocks))
# url_snap = html.xpath('//*[@class="c-showurl"]')
# print(len(url_snap))
# for i in url_snap:
#     print(i.xpath('string()'))



# for j in range(len(result_blocks)):
#     url_snap = result_blocks[j].xpath('//a[@class="c-showurl"]')
#     print(len(url_snap))
    # for i in url_snap:
    #     print(i.xpath('string()'))



# a = 'https://www.baidu.com/link?url=eiBvVQz9fz_hZj6SiLRqwzvJD5ILeGy9ZEB5BIBxrJO&wd=&eqid=cd4b96c900097dcd000000065cbe8ad1'
# g = 'http://github.com'

# r = requests.get(a)
# print(r.status_code)
# print(r.history)

# r = requests.head(g, headers=headers, allow_redirects=True)
# print(r.url)

# response = urllib.request.urlopen(a)
# realurl = response.info()
# print(realurl)

# response = urllib.request.urlopen(a)
# print(response.geturl())

# m = 'https://www.baidu.com/link?url='
# s = 'https://www.baidu.com/link?url=02123232323232323232323232323232323'
# if m in s:
#     print(123)
import requests
from lxml import etree
import time
import threading
from queue import Queue
import re





PAGE_SIZE = 30      # 一個搜尋頁面要顯示幾個結果
PAGE_NUM = 1        # 要跑幾個搜尋頁面
# POOL_SIZE = 30


# 參數：search_result: 搜尋結果 { 'url': 搜尋結果url(string) , 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), 'rank': 排名(int) }
#       redirect_results: 重新導向後結果，queue of { 'url': 重新導向後url(string) , 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), 'rank': 排名(int) }
#       thread_count: 檢查碼(int)
# 功能：將百度搜尋結果所得之url重新導向，得到實際url，將 search_result 之 url 更新後 put 進 redirect_results
# 回傳：無
def redirect(search_result, redirect_results, keyword='', thread_count=-1):
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Mobile Safari/537.36'
        }
        response = requests.get(search_result['url'], headers=headers, allow_redirects=False)
        url = response.headers['Location']
        # print(url)
        search_result['url'] = url
        redirect_results.put(search_result)
    except Exception as e:
        print('Exception:', e, ', By', search_result['url'])
        time.sleep(1)
        log_file = open('Redirect Exception.txt', 'a', encoding='UTF-8')
        log_str = str(e) + '\n' + 'Occur time: ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n' + \
            'URL: ' + search_result['url'] + '\n' + 'Keyword: ' + keyword + '\n' + 'Rank: ' + str(search_result['rank']) + \
                '\n' + 'Title: ' + search_result['title'] + '\n\n'
        search_result['url'] = 'error'
        log_file.write(log_str)
        log_file.close()
        redirect_results.put(search_result)


# 參數：word: 搜尋關鍵字(string)
#       page_num: 要跑幾個搜尋頁面(int)
#       page_size: 一個搜尋頁面要顯示幾個結果(int)
# 功能：將關鍵字進行百度搜尋，並回傳搜尋結果
# 回傳：搜尋結果列表 [ { 'url': 重新導向後搜尋結果url(string), 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), rank': 排名(int) } ]
def get_baidu_urls(word, page_num=PAGE_NUM, page_size=PAGE_SIZE):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Mobile Safari/537.36'
    }
    data = {
        'ie': 'utf-8',
        'wd': word,
        'pn': 0,
        'rn': page_size
    }
    count = 0
    search_results = []
    print(word)
    for i in range(page_num):
        # print('get once')
        try:
            data['pn'] = i * 10
            # print(data)
            time_string = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            r = requests.get('https://www.baidu.com/s', params=data, headers=headers)
        except Exception as e:
            print(e)
            print('Exception occur during searching keyword \"', word, '\".')
            log_file = open('Searching Exception.txt', 'a', encoding='UTF-8')
            log_str = str(e) + '\n' + 'Occur time: ' + str(time_string) + '\n' + 'Keyword: ' + str(word.encode('utf-8').decode('utf-8')) + '\n\n'
            log_file.write(log_str)
            log_file.close()
        else:
            html = etree.HTML(r.text, etree.HTMLParser())
            result_blocks = html.xpath('//h3')
            snap_urls = html.xpath('//*[@class="c-showurl"]')
            for j in range(len(result_blocks)):
                title = result_blocks[j].xpath('string(.)').strip()
                xhref_path = result_blocks[j].xpath('a/@href')
                link = ''
                if len(xhref_path) > 0:
                    link = result_blocks[j].xpath('a/@href')[0]
                # link = redirect(link)
                search_results.append({ 'url': link, 'time': time_string, 'rank': (10 * i + j + 1), 'title': title })
        
    # multithread
    threads = []
    redirect_results = Queue()
    count = 0
    for search_result in search_results:
        # print(search_result)
        if 'https://www.baidu.com/link?url=' in search_result['url'] or 'http://www.baidu.com/link?url=' in search_result['url']:
            threads.append(threading.Thread(target=redirect, args=(search_result,redirect_results,word,count)))
            threads[count].start()
            count += 1
        else:
            redirect_results.put(search_result)
    for thread in threads:
        thread.join()
    ret = []
    while not redirect_results.empty():
        ret.append(redirect_results.get())
    for r in ret:
        print(r)
    # ret = search_results
    return ret
        
def main():
    ret = get_baidu_urls('亚美am8电游娱乐官网')

if __name__ == '__main__':
    main()