import requests
from lxml import etree
import time
import threading
from queue import Queue
import re
# import random
# import os
# from multiprocessing import Pool

PAGE_SIZE = 30      # 一個搜尋頁面要顯示幾個結果
PAGE_NUM = 1        # 要跑幾個搜尋頁面
# POOL_SIZE = 30


# 參數：search_result: 搜尋結果 { 'url': 搜尋結果url(string) , 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), 'rank': 排名(int) }
#       redirect_results: 重新導向後結果，queue of { 'url': 重新導向後url(string) , 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), 'rank': 排名(int) }
#       thread_count: 檢查碼(int)
# 功能：將百度搜尋結果所得之url重新導向，得到實際url，將 search_result 之 url 更新後 put 進 redirect_results
# 回傳：無
def redirect(search_result, redirect_results, keyword='', thread_count=-1):
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
        }
        # print('fuck2')
        response = requests.get(search_result['url'], headers=headers, allow_redirects=False)
        url = response.headers['Location']
        # print('fuck')
        print(url)
        search_result['url'] = url
        redirect_results.put(search_result)

        # time.sleep(random.uniform(0.05, 0.2))
        # print(thread_count)
        # print(thread_count, 'try url', search_result['url'])
        # res = requests.get(search_result['url'], timeout=10) #todo: 只get header
        # tmp = search_result['url']
        # print(thread_count, res.url, '\n')
        # search_result['url'] = res.url
        # print(thread_count, 'RES', search_result['url'], '\n')
        # redirect_results.put(search_result)

    except Exception as e:
        print('Exception:', e, ', By', search_result['url'])
        time.sleep(1)
        #redirect(search_result, redirect_results, thread_count)
        log_file = open('Redirect Exception.txt', 'a', encoding='UTF-8')
        log_str = str(e) + '\n' + 'Occur time: ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n' + \
            'URL: ' + search_result['url'] + '\n' + 'Keyword: ' + keyword + '\n' + 'Rank: ' + str(search_result['rank']) + \
                '\n' + 'Title: ' + search_result['title'] + '\n\n'
        search_result['url'] = 'error'
        log_file.write(log_str)
        log_file.close()
        redirect_results.put(search_result)
    # return search_result


# 參數：word: 搜尋關鍵字(string)
#       page_num: 要跑幾個搜尋頁面(int)
#       page_size: 一個搜尋頁面要顯示幾個結果(int)
# 功能：將關鍵字進行百度搜尋，並回傳搜尋結果
# 回傳：搜尋結果列表 [ { 'url': 重新導向後搜尋結果url(string), 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), rank': 排名(int) } ]
def get_baidu_urls(word, page_num=PAGE_NUM, page_size=PAGE_SIZE):
    headers = {
        'pragma': 'no-cache',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.8',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'cache-control': 'no-cache',
        'connection': 'keep-alive',
    }
    data = {
        'ie': 'utf-8',
        'wd': word,
        'pn': 0,
        'rn': page_size
    }
    count = 0
    search_results = []
    print(word)
    for i in range(page_num):
        # print('get once')
        try:
            data['pn'] = i * 10
            # print(data)
            time_string = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            r = requests.get('https://www.baidu.com/s', params=data, headers=headers)
        except Exception as e:
            print(e)
            print('Exception occur during searching keyword \"', word, '\".')
            log_file = open('Searching Exception.txt', 'a', encoding='UTF-8')
            log_str = str(e) + '\n' + 'Occur time: ' + str(time_string) + '\n' + 'Keyword: ' + str(word.encode('utf-8').decode('utf-8')) + '\n\n'
            log_file.write(log_str)
            log_file.close()
        else:
            html = etree.HTML(r.text, etree.HTMLParser())
            result_blocks = html.xpath('//h3')
            snap_urls = html.xpath('//*[@class="c-showurl"]')
            for j in range(len(result_blocks)):
                title = result_blocks[j].xpath('string(.)').strip()
                xhref_path = result_blocks[j].xpath('a/@href')
                link = ''
                if len(xhref_path) > 0:
                    link = result_blocks[j].xpath('a/@href')[0]
                # link = redirect(link)
                search_results.append({ 'url': link, 'time': time_string, 'rank': (10 * i + j + 1), 'title': title })
        
        # else:
        #     for j in range(len(result_blocks)):
        #         try:
        #             title = result_blocks[j].xpath('string(.)').strip()
        #             link = result_blocks[j].xpath('a/@href')[0]
        #             link = redirect(link)
        #             search_results.append({ 'link': link, 'time': time_string, 'rank': (10 * i + j + 1) })

        #             title = result_blocks[j].xpath('string(.)').strip()
        #             url = result_blocks[j].xpath('a/@href')[0] # Exception has occurred: IndexError, list index out of range
        #             snap_url = snap_urls[j].xpath('string()')
        #             print(j)
        #             print(snap_url)
        #             search_results.append({ 'url': snap_url, 'time': time_string, 'rank': (10 * i + j + 1) })
        #         except Exception as e:
        #             print(e)
        #             print('Exception occur during searching keyword \"', word, '\", No.', j + 1, 'block')
        #             log_file = open('Searching Exception.txt', 'a', encoding='UTF-8')
        #             log_str = str(e) + '\n' + 'Occur time: ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n' + 'Keyword: ' + str(word.encode('utf-8').decode('utf-8')) + '\n' + 'Block: ' + str(j + 1) + '\n\n'
        #             log_file.write(log_str)
        #             log_file.close()
        #         else:
        #             print(10*i + j)
        #             count += 1
        #             print(count)
    
    # multithread
    threads = []
    redirect_results = Queue()
    count = 0
    for search_result in search_results:
        # print(search_result)
        if 'https://www.baidu.com/link?url=' in search_result['url'] or 'http://www.baidu.com/link?url=' in search_result['url']:
            threads.append(threading.Thread(target=redirect, args=(search_result,redirect_results,word,count)))
            threads[count].start()
            count += 1
        else:
            redirect_results.put(search_result)
    for thread in threads:
        thread.join()
    ret = []
    while not redirect_results.empty():
        ret.append(redirect_results.get())
    for r in ret:
        print(r)
    # ret = search_results
    return ret


# 目前無用 #
# ThreadPool
# def pool():
#     task_pool = Pool(processes=POOL_SIZE)
#     results = []
#     for search_result in search_results:
#         results.append(task_pool.apply_async(redirect, args=(search_result,)).get())
#     task_pool.join()
#     return results


# 參數：search_result: 搜尋結果列表 [ { 'url': 重新導向後url(string) , 'time': 搜尋時間('%Y-%m-%d %H:%M:%S'), 'rank': 排名(int) } ]
#       domain_list: [ 該品牌旗下的域名 ]
# 功能：檢查搜尋結果 url 是否屬於該品牌 domain，若是，則回傳該筆資料
# 回傳：比對成功之結果 [ { 'rank': 排名(int), 'domain': 比對成功之域名(string), 'time': 搜尋時間('%Y-%m-%d %H:%M:%S') } ]
def domain_pairing(search_result, domain_list):
    rank = 0
    match_list = []
    # print(domain_list)
    for r in search_result:
        # print(r['url'])
        for d in domain_list:
            if d in r['url']:
                match_list.append({ 'rank': r['rank'], 'domain': d, 'time': r['time'] })
    return match_list


# 測試區
def main():
    get_baidu_urls('亚洲城88线上娱乐')
    # for i in range(50000):
    #     a = time.time()
    #     print('########################### %d' % i)
    #     print(a)
    #     search_results = get_baidu_urls('dafabet手机客户端')
    #     print(time.time() - a)
    
    #     for search_result in search_results:
    #         print(search_result)
    
    # print(len(search_results))
    # print(time.time() - a)


if __name__ == '__main__':
    main()
