# version: 1.0.0

from mongodb_conn import MongodbConn
import time


class MongodbIO:
    def __init__(self, db_name, coll_name):
        self.db_name = db_name
        self.coll_name = coll_name

    # 參數：match: 篩選條件(query)(選填)
    # 功能：在coll_name資料表中找尋所有符合match條件的document
    # 回傳：document cursor
    def get_doc(self, match={}):
        conn = MongodbConn(self.db_name)
        coll = conn.collection(self.coll_name)
        ret = coll.find(match)
        conn.release()
        return ret


    # 參數：field_name: 欲找尋的欄位名稱(string), match: 篩選條件(query)(選填)
    # 功能：在coll_name資料表中找尋field_name欄位所有不相同且符合match條件的item
    # 回傳：所有彼此不相同的field_name欄位(list)
    def get_distinct(self, field_name, match={}):
        conn = MongodbConn(self.db_name)
        coll = conn.collection(self.coll_name)
        ret = coll.distinct(field_name, match)
        conn.release()
        return ret

    # 參數：data_list: 欲新增/更新的document列表(dict list)
    # 功能：列表中的每個document若存在_id欄位且已存在collection中則更新其他欄位，否則新增一筆document，並印出更新訊息。
    # 回傳：無
    def insert_or_update(self, data_list):
        conn = MongodbConn(self.db_name)
        coll = conn.collection(self.coll_name)
        for i in data_list:
            if '_id' in i:
                print(coll.replace_one({'_id': i['_id']}, i, True))
            else:
                print(coll.insert_one(i))
        conn.release()

    # 參數：field_name: 欲更改的欄位(string), new_value: 更改後的值, match: 篩選條件(query)(選填)
    # 功能：列表中的每個document若存在_id欄位且已存在collection中則更新其他欄位，否則新增一筆document，並印出更新訊息。
    # 回傳：無
    def set_item_value(self, field_name, new_value, match={}):
        conn = MongodbConn(self.db_name)
        coll = conn.collection(self.coll_name)
        x = coll.update_many(match, { '$set': { field_name: new_value } })
        print(x.modified_count, 'documents updated.')

    # 參數：無
    # 功能：取得當前時間
    # 回傳：當前時間(float)
    @staticmethod
    def get_create_time():
        return time.time()


# 測試區
def main():
    db = MongodbIO('check-list-database', 'preference')
    
    # db.set_item_value('a', '123', {'_id': '1'})
    
    # print(db.get_distinct('_id'))
    # a = db.get_doc({'_id': '4'})
    # print(a[1]['a'])
    # db.insert_or_update([{'_id': '4', 'a': ['456', 1, 1, '123']}, {'_id': 1, 'a': '123'}, {'_id': '3', 'a': '456'}, {'_id': '2', 'b': '123'}])
    
    # a = {'_id': '', 'a': '456'}
    # if 'a' not in a:
    #     print('1')
    # else:
    #     print('0')
    # print(get_distinct('ian_test', 'a'))
    
    # i = coll.replace_one({'_id': a['_id']}, a, True)
    # i = coll.find_one_and_update({'_id': '1'}, {'$set': {'a': '321'}})
    # i = coll.insert_many([{'_id': 3, 'a': '123'}])


if __name__ == '__main__':
    main()


# Sample code #
# collection = MongodbIO('check-list-database', 'ian_test')

# a = collection.get_doc({'_id': '1'})
# for index in a:
#     print(index)

# b = collection.get_distinct('_id', {'score': {'$lt': 20}})
# for index in b:
#     print(index)

# collection.insert_or_update([{'a': '456'}, {'_id': 1, 'a': '123'}, {'_id': '3', 'a': '456'}, {'_id': '2', 'b': '123'}])

# db.set_item_value('a', '123', {'_id': '1'})

# t = collection.get_create_time()

# 關於mongoDB query的寫法可參考：https://docs.mongodb.com/manual/tutorial/query-documents/
# 其中要注意query中dict的key在python內需為string型態，因此需要加上單/雙引號

