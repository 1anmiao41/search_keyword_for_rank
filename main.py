import rank_finder
import baidu_rank_mongodb
import time
# database version: 2.0

def main():
    a = time.time()
    new_serial_num = baidu_rank_mongodb.update_rank_serial_num()                # db 中 preference 內 rank_serial_number +1
    brand_list = baidu_rank_mongodb.get_brand_list()                            # 取得品牌名列表
    
    for brand in brand_list:
        domain_list = baidu_rank_mongodb.get_domain_list(brand)                 # 取得該品牌旗下域名列表
        print(domain_list)
        keyword_list = baidu_rank_mongodb.get_keyword_list(brand)               # 取得該品牌旗下所有關鍵字

        for keyword in keyword_list:
            search_result = rank_finder.get_baidu_urls(keyword)                 # 取得關鍵字搜尋結果列表 [ { 'url', 'time', rank' } ]
            rank_list = rank_finder.domain_pairing(search_result, domain_list)  # 比對搜尋結果 url 和域名，回傳比對成功列表 [ { 'rank', 'domain', 'time' } ]
            baidu_rank_mongodb.rank_update(keyword, rank_list)                  # 將比對成功結果 insert db
    
    print('rank_serial_number has been updated to %d.' % new_serial_num)
    b = time.time()
    print('Time spent:', b - a, 'seconds.')


if __name__ == '__main__':
    main()